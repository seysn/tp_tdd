package iut.tdd;

public class Convert {
	public static String num2text(String input) {
	    String res = null;
	    if (Integer.parseInt(input)>=0 & Integer.parseInt(input)<=16) {
            res = unite(input);
        } else if (Integer.parseInt(input)>=17 & Integer.parseInt(input)<=19) {
            res = dizaine(input);
        } else if (Integer.parseInt(input)>=20 & Integer.parseInt(input)<=69) {
            res = vingtToSoixanteNeuf(input);
        }
	    return res;
	}
	public static String text2num(String input) {
		return null;
	}
	
	public static String unite(String input) {
	    String res= null;
	    switch (Integer.parseInt(input)) {
            case 0:
                res="zéro";
                break;
            case 1:
                res="un";
                break;
            case 2:
                res="deux";
                break;
            case 3:
                res="trois";
                break;
            case 4:
                res="quatre";
                break;
            case 5:
                res="cinq";
                break;
            case 6:
                res="six";
                break;
            case 7:
                res="sept";
                break;
            case 8:
                res="huit";
                break;
            case 9:
                res="neuf";
                break;
            case 10:
                res="dix";
                break;
            case 11:
                res="onze";
                break;
            case 12:
                res="douze";
                break;
            case 13:
                res="treize";
                break;
            case 14:
                res="quatorze";
                break;
            case 15:
                res="quinze";
                break;
            case 16:
                res="seize";
                break;
            default:
                break;
        }
	    return res;
	}
	
	public static String dizaine(String input){
	    String res = null;
	    res = unite("10")+"-"+unite(""+input.charAt(1));
	    return res;
	}

	public static String vingtToSoixanteNeuf(String input){
	    String res = null;
	    switch (Integer.parseInt(""+input.charAt(0))) {
            case 2:
                res="vingt";
                break;
            case 3:
                res="trente";
                break;
            case 4:
                res="quarante";
                break;
            case 5:
                res="cinquante";
                break;
            case 6:
                res="soixante";
                break;
            default:
                break;
        }
	    
	    switch (Integer.parseInt(""+input.charAt(1))) {
            case 1:
                res+=" et "+unite("1");
                break;
            default:
                res+="-"+unite(""+input.charAt(1));
                break;
        }
	    return res;
	}
	
}