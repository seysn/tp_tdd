package iut.tdd;

import junit.framework.Assert;

import org.junit.Test;

public class TestConvert {
	@Test
	public void test_num2text_zero () {		
		Assert.assertEquals("zéro", Convert.num2text("0"));
	}
	
	@Test
	public void test_num2text_trois(){
	    Assert.assertEquals("trois", Convert.num2text("3"));
	}
	
	@Test
	public void test_num2text_six(){
	    Assert.assertEquals("six", Convert.num2text("6"));
	}
	
	@Test
	public void test_num2test_dix(){
        Assert.assertEquals("dix", Convert.num2text("10"));
	}
	
	@Test
	public void test_num2test_onze(){
        Assert.assertEquals("onze", Convert.num2text("11"));
	}
	
	@Test
	public void test_num2test_seize(){
	    Assert.assertEquals("seize", Convert.num2text("16"));
	}
	
	@Test
	public void test_num2test_dixhuit(){
	    Assert.assertEquals("dix-huit", Convert.num2text("18"));
	}
	
	@Test
	public void test_num2test_trenteetun(){
	    Assert.assertEquals("trente et un", Convert.num2text("31"));
	}
	
	@Test
	public void test_num2test_cinquantequatre(){
	    Assert.assertEquals("cinquante-quatre", Convert.num2text("54"));
	}
}
